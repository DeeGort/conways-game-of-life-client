import { Rule } from './../rule/cell';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Cell } from '../cell/cell';
import { GameService } from '../game/game.service';
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css'],
  providers: [ GameService ]
})
export class GridComponent implements OnInit, OnDestroy {
  private width: number;
  private height: number;
  private offsetWidth: number;
  private offsetHeight: number;
  private generation: number = 10;
  private currentGeneration: number = 0;
  private rule: Rule;
  private buffer: Array<Array<Cell>>;
  private newBuffer: Array<Array<Cell>>;
  private isStarted: boolean = false;
  private frame: number = 0;
  private firstStart: boolean = true;
  private updateRate: number = 100;
  public grid: Array<Array<Cell>>;

  @Input() lifObservable: Observable<Array<Cell>>;
  @Input() buttonControlObservable: Observable<string>;
  @Output() generationCounter: EventEmitter<number> = new EventEmitter();

  private subscriptions: Subscription[] = [];

  constructor(private game: GameService) { }

  ngOnInit(): void {
    this.initGrid();
    this.subscriptions.push(this.buttonControlObservable
      .subscribe(button => {this.control(button)}));
    this.subscriptions.push(this.lifObservable
      .subscribe((lif: any) => {
        // Re(init)
        this.clearGrid();
        this.currentGeneration = 0;
        this.isStarted = false;
        this.firstStart = true;
        this.frame = 0;   
        this.generationCounter.emit(this.currentGeneration);

        // Show zero generation
        this.buffer =  Array.of(lif.cells);
        this.rule = lif.rule;
        this.fillGrid();

        // Fetch more generations
        this.game
          .getGenerations(lif.cells, lif.rule, this.generation)
          .subscribe((data) => {
            this.buffer = data.json();

            // Block
            setTimeout(() => {
              this.firstStart = false;
            }, 500);
          });
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  /* Gameplay control handler */
  private control(buttonPress: string): void {
    if (this.firstStart) { return; }
    this[buttonPress]();
  }

  /* Gameplay handlers */
  private start(): void {
    if (!this.isStarted) {
      this.isStarted = true;
      this.process();
    }
  }

  private stop(): void {
    this.isStarted = false;
  }

  private next(): void {
    this.clearGrid();
    this.frame++;
    if (this.frame -1 === 0) {
      this.fetch();
    }
    if (this.frame >= this.generation) {
      this.buffer = this.newBuffer;
      this.frame = 0;
    }
    this.fillGrid();
    this.generationCounter.emit(++this.currentGeneration);
  }

  private process(): void {
    let scheduler = setInterval(() => {
      if (!this.isStarted) {
        clearInterval(scheduler);
        return;
      }
      this.next();
    }, this.updateRate);
  }

  /* Network service handler */
  private fetch(): void {
    this.game
    .getGenerations(this.buffer[this.generation - 1], this.rule, this.generation)
    .subscribe(data => {
      this.newBuffer = data.json();
    });
  }

  /* UI grid handlers */
  private initGrid(): void {
    const innerHeight = (document.body.clientHeight - 47) / 15;
    const innerWidth = (document.body.clientWidth) / 15;
    this.width = Math.ceil(innerWidth);
    this.height = Math.ceil(innerHeight);
    this.offsetWidth = Math.floor(this.width / 2);
    this.offsetHeight = Math.floor(this.height / 2);

    this.grid = [];
    for (let i = 0; i < this.height; i++) {
      this.grid[i] = [];
      for (let j = 0; j < this.width; j++) {
        this.grid[i][j] = new Cell(i, j, false);
      }
    }
  }

  private fillGrid() {
    this.buffer[this.frame].forEach(cell => {
      const x = cell.x + this.offsetWidth;
      const y = cell.y + this.offsetHeight;

      if ((x >= 0 && y >= 0) && (x < this.width && y < this.height)) {
        this.grid[y][x].state = true;
      }
    });
  }

  private clearGrid(): void {
    for (let i = 0; i < this.height; i++) {
      for (let j = 0; j < this.width; j++) {
        this.grid[i][j].state = false;
      }
    }
  }

}

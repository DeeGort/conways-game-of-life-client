export class StringProcessor {
    
  private index: number = 0;
  private lines: Array<string>;

  constructor(
    str: String,
  ) {

    // Break into lines
    if (str.indexOf('\r\n') !== -1) {
        this.lines = str.split('\r\n');
    } else if (str.indexOf('\r') !== -1) {
        this.lines = str.split('\r');
    } else if (str.indexOf('\n') !== -1) {
        this.lines = str.split('\n');
    }

    // Trim lines
    this.lines.forEach((line, index) => {
        this.lines[index] = line.trim();
    });
  }

  nextLine(): string {
    return this.lines ? this.lines[this.index++] : '';
  }

  line(): string {
    return this.lines ? this.lines[this.index] : ''
  }

  hasNext(): boolean {
    return this.lines && this.index + 1 < this.lines.length; 
  }

  getIndex(): number {
    return this.index;
  }
}

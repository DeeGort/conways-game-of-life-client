import { LifBuilder } from './lifBuilder';
import { StringProcessor } from './stringProcessor';

export class LifProcessor {

    constructor(
        private lifBuilder: LifBuilder
    ) { }

    process(lines: StringProcessor): Object {
        this.processHeader(lines);
        this.processDescriptions(lines);
        this.processRule(lines);
        this.processLines(lines);

        return this.lifBuilder.finish();
    }

    /* Processors */
    private processHeader(lines: StringProcessor): void {
        if (lines.hasNext() && this.isHeader(lines.nextLine())) {
            // Do nothing
        } else {
            throw new Error(`Error: Invalid file header at: ${lines.getIndex()} row. Corrupt value: ${lines.line()}`);
        }
    }

    private processDescriptions(lines: StringProcessor): void {
        do {
            lines.nextLine();
            // Do nothing
        } while (lines.hasNext() && this.isDescription(lines.line()));
    }

    private processRule(lines: StringProcessor): void {
        if (this.isNormalRule(lines.line())) {
            this.lifBuilder.insertRule();
        } else if (this.isCustomRule(lines.line())) {
            const rule = this.parserRule(lines.line());
            this.lifBuilder.insertRule(rule[0], rule[1]);
        } else {
            throw new Error(`Error: Invalid rule at: ${lines.getIndex()} row. Corrupt value: ${lines.line()}`);
        }
    }

    private processLines(lines: StringProcessor): void {
        do {
            lines.nextLine();
            if (this.isOffser(lines.line())) {
                const offset = this.parserOffset(lines.line());
                this.lifBuilder.setOffset(offset[0], offset[1]);
            } else if (this.areCells(lines.line())) {
                this.parserCells(lines.line()).forEach(cell => {
                    if (cell === '*') {
                        this.lifBuilder.insertCell();
                        this.lifBuilder.incrementCurrentLinePositon();
                    } else {
                        this.lifBuilder.incrementCurrentLinePositon();
                    }
                });
                this.lifBuilder.incrementCurrentLine();
            } else {
                throw new Error(`Error: Invalid line at: ${lines.getIndex()} row. Corrupt value: ${lines.line()}`);
            }

        } while (lines.hasNext());
    }

    /* Validators */
    private isHeader(line: string): boolean {
        return /^#Life\s+\d+\.\d+$/.test(line);
    }

    private isDescription(line: string): boolean {
        return /^#D.*$/.test(line);
    }

    private isNormalRule(line: string): boolean {
        return /^#N\s*$/.test(line);
    }

    private isCustomRule(line: string): boolean {
        return /^#R\s*\d+[\/]\d+$/.test(line);
    }

    private isOffser(line: string): boolean {
        return /^#P\s*-?\d+\s+-?\d+$/.test(line);
    }

    private areCells(line: string): boolean {
        return /^([\*]|[\.])*$/.test(line);
    }

    /* Parsers */
    private parserRule(line: string): Array<string> {
        return line.replace('#R', '').trim().split('/');
    }

    private parserOffset(line: string): Array<number> {
        return line.replace('#P', '').trim().split(' ').map(a => parseInt(a));
    }

    private parserCells(line: string): Array<string> {
        return line.split('');
    }
}

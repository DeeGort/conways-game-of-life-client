export class LifBuilder {
    lif: any = {
        rule: {
            survival: '',
            birth: ''
        },
        cells: [ ]
    };

    // x position start
    linePositonOffset: number = 0;
    // x position
    currentLinePositon: number = 0;
    // y position
    currentLine: number = 0;

    insertRule(survival: string = '23', birth: string = '3'): void {
        this.lif.rule.survival = survival;
        this.lif.rule.birth = birth;
    }

    insertCell(): void {
        this.lif.cells.push({
            x: this.currentLinePositon + this.linePositonOffset,
            y: this.currentLine
        });
    }

    setOffset(x: number, y: number): void {
        this.linePositonOffset = x;
        this.currentLinePositon = 0;
        this.currentLine = y;
    }

    incrementCurrentLinePositon(): void {
        this.currentLinePositon++;
    }

    incrementCurrentLine(): void {
        this.currentLine++;
        this.currentLinePositon = 0;
    }

    finish(): Object {
        return this.lif;
    }
}
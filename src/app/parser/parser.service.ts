import { LifBuilder } from './lifBuilder';
import { LifProcessor } from './lifProcessor';
import { Injectable } from '@angular/core';
import { StringProcessor } from "./stringProcessor";

@Injectable()
export class ParserService {

  constructor() { }

  parse(lifContent: string): Object {
    let lines = new StringProcessor(lifContent);
    let builder = new LifBuilder();
    let lifProcessor = new LifProcessor(builder);

    let result: Object;
    try {
      result = lifProcessor.process(lines);
    } catch (error) {
      console.log(error);
    }

    return result;
  }

}

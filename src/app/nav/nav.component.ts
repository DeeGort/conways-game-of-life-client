import { Observable } from 'rxjs/Observable';
import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

  @Input() generationCounterObservable: Observable<number>;
  @Output() buttonPressed: EventEmitter<string> = new EventEmitter();
  @Output() fileOpened: EventEmitter<string> = new EventEmitter();

  onStart(): void {
    this.buttonPressed.emit('start');
  }

  onStop(): void {
    this.buttonPressed.emit('stop');
  }

  onNext(): void {
    this.buttonPressed.emit('next');
  }

  onFileUploaded(file: string) {
    this.fileOpened.emit(file);
  }

}

import { Cell } from './cell/cell';
import { Component, OnInit } from '@angular/core';
import { ParserService } from './parser/parser.service';
import { Subject } from "rxjs/Subject";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ ParserService ]
})
export class AppComponent implements OnInit {

  private lifSubject: Subject<Object>;
  private buttonControlSubject: Subject<string>;
  private generationCounterBehaviorSubject: BehaviorSubject<number>;

  constructor(
    private parser: ParserService
  ) { }

  ngOnInit(): void {
    this.lifSubject = new Subject<Array<Cell>>();
    this.buttonControlSubject = new Subject<string>();
    this.generationCounterBehaviorSubject = new BehaviorSubject<number>(0);
  }

  onFileOpened(file: string) {
    let lif = this.parser.parse(file);
    if (lif) {
      this.lifSubject.next(lif);
    }
  }

  onButtonPressed(button: string) {
    this.buttonControlSubject.next(button);
  }

  onGenerationChanged(generationCount: number) {
    this.generationCounterBehaviorSubject.next(generationCount);
  }

  public get lifObservable(): Observable<Object> {
    return this.lifSubject.asObservable();
  }

  public get buttonControlObservable(): Observable<string> {
    return this.buttonControlSubject.asObservable();
  }

  public get generationCounterObservable(): Observable<number> {
    return this.generationCounterBehaviorSubject.asObservable();
  }

}

import { Cell } from './../cell/cell';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.css']
})
export class UploaderComponent {
  @Output() fileChange: EventEmitter<string> = new EventEmitter();

  openFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      if (!file){ return; }

      var reader = new FileReader();
      reader.onload = (e: any) => {
        this.fileChange.emit(e.target.result);
      }
      reader.readAsText(file);
    }
  }

  open() {
    document.getElementById('fileInput').click();
  }

}

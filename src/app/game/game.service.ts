import { Rule } from './../rule/cell';
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

@Injectable()
export class GameService {

  private gameUrl = 'api/next?';
  private param = 'generation=';
  private headers = new Headers({'Content-Type': 'application/json'});
  private searchParams = new URLSearchParams();

  constructor(
    private http: Http
  ) { }

  getGenerations(cells: Array<Object>, rule: Rule, generation: number) {
    
    this.searchParams.set('generation', String(generation));
    this.searchParams.set('survival', String(rule.survival));
    this.searchParams.set('birth', String(rule.birth));

    return this.http.post(this.gameUrl + this.searchParams.toString(), cells, {headers: this.headers});
  }

}
